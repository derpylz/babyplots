test_that("creating an imageStack visualization works", {
    p <- imgStack(
        "../imgStack_test_2.tif",
        backgroundColor = "black",
        zScale = 0.1,
        channelColors = "white",
        channelOpacities = 1
    )
    expect_s3_class(p, "Babyplot")
    expect_identical(length(p$plots), 1L)
    expect_identical(p$zScale, 0.1)
    expect_identical(p$backgroundColor, "black")
    expect_s3_class(p$plots[[1]], "ImageStack")
    expect_identical(p$plots[[1]]$plotType, "imageStack")
})
