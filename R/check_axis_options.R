#' Check axis options
#'
#' @description Check the axis options for consistency and correct them if
#' necessary and possible. This function is used internally by the babyplots
#' package.
#'
#' @param axis_options a list of axis options:
#' axisColors, axisLabels, showAxes, tickBreaks, showTickLines, tickLineColors
#'
#' @return The corrected list of axis options
#' @keywords internal
#' @noRd
check_axis_options <- function(axis_options) {
    axis_options$showAxes <- check_showAxes(axis_options$showAxes)
    axis_options$axisLabels <- check_axisLabels(axis_options$axisLabels)
    axis_options$axisColors <- check_axisColors(axis_options$axisColors)
    axis_options$tickBreaks <- check_tickBreaks(axis_options$tickBreaks)
    axis_options$showTickLines <- check_showTickLines(
        axis_options$showTickLines
    )
    axis_options$tickLineColors <- check_tickLineColors(
        axis_options$tickLineColors
    )

    return(axis_options)
}

is_true_or_false <- function(x) {
    # checks for TRUE or FALSE without coercion to logical
    if (isTRUE(x) || isFALSE(x)) {
        return(TRUE)
    }
    return(FALSE)
}

#' Check showAxes option
#'
#' @description Check the showAxes option for consistency and correct it if
#' necessary and possible. This function is used internally by the babyplots
#' package.
#'
#' @param showAxes the showAxes option
#' @return The corrected showAxes option
#' @keywords internal
#' @noRd
check_showAxes <- function(showAxes) {
    if (length(showAxes) == 1) {
        if (!is_true_or_false(showAxes)) {
            stop(
                "showAxes must be a logical value. ",
                "Got ", class(showAxes), " instead."
            )
        }
        showAxes <- rep(showAxes, times = 3)
    } else if (length(showAxes) != 3) {
        stop(
            "Incorrect number of axis display options. ",
            "Must be either one or three, not ",
            length(showAxes)
        )
    }
    # check if all elements are logical
    if (!all(sapply(showAxes, is_true_or_false))) {
        stop(
            "showAxes must be a logical value. ",
            "Got ", class(showAxes), " instead."
        )
    }
    return(showAxes)
}

#' Check axisLabels option
#' @description Check the axisLabels option for consistency and correct it if
#' necessary and possible. This function is used internally by the babyplots
#' package.
#' @param axisLabels the axisLabels list
#' @return The corrected axisLabels list
#' @keywords internal
#' @noRd
check_axisLabels <- function(axisLabels) {
    if (length(axisLabels) == 1) {
        axisLabels <- as.list(rep(axisLabels, times = 3))
    } else if (length(axisLabels) != 3) {
        stop(
            "Incorrect number of axis label options. ",
            "Must be either one or three, not ",
            length(axisLabels)
        )
    }
    # check if all elements are characters
    if (!all(sapply(axisLabels, is.character))) {
        stop(
            "axisLabels must contain character values. ",
            "Got ", class(axisLabels), " instead."
        )
    }
    # make sure axisLabels is a list
    axisLabels <- as.list(axisLabels)
    return(axisLabels)
}

#' Check axisColors option
#' @description Check the axisColors option for consistency and correct it if
#' necessary and possible. This function is used internally by the babyplots
#' package.
#' @param axisColors the axisColors list
#' @return The corrected axisColors list
#' @keywords internal
#' @noRd
check_axisColors <- function(axisColors) {
    if (length(axisColors) == 1) {
        axisColors <- rep(axisColors, times = 3)
    } else if (length(axisColors) != 3) {
        stop(
            "Incorrect number of axis color options. ",
            "Must be either one or three, not ",
            length(axisColors)
        )
    }
    # check if all elements are characters
    if (!all(sapply(axisColors, is.character))) {
        stop(
            "axisColors must contain character values. ",
            "Got ", class(axisColors), " instead."
        )
    }
    # make sure axisColors is a list
    axisColors <- as.list(axisColors)
    return(axisColors)
}

#' Check tickBreaks option
#' @description Check the tickBreaks option for consistency and correct it if
#' necessary and possible. This function is used internally by the babyplots
#' package.
#' @param tickBreaks the tickBreaks option
#' @return The corrected tickBreaks option
#' @keywords internal
#' @noRd
check_tickBreaks <- function(tickBreaks) {
    # tickBreaks must be a single number or a vector of three numbers
    if (length(tickBreaks) == 1) {
        if (!is.numeric(tickBreaks)) {
            stop(
                "tickBreaks must be a numeric value. ",
                "Got ", class(tickBreaks), " instead."
            )
        }
        tickBreaks <- rep(tickBreaks, times = 3)
    } else if (length(tickBreaks) != 3) {
        stop(
            "Incorrect number of tick break options. ",
            "Must be either one or three, not ",
            length(tickBreaks)
        )
    }
    # check if all elements are numeric
    if (!all(sapply(tickBreaks, is.numeric))) {
        stop(
            "tickBreaks must contain numeric values. ",
            "Got ", class(tickBreaks), " instead."
        )
    }
    return(tickBreaks)
}

#' Check showTickLines option
#' @description Check the showTickLines option for consistency and correct it if
#' necessary and possible. This function is used internally by the babyplots
#' package.
#' @param showTickLines the showTickLines option
#' @return The corrected showTickLines option
#' @keywords internal
#' @noRd
check_showTickLines <- function(showTickLines) {
    if (length(showTickLines) == 3) {
        dimc <- 1
        for (option in showTickLines) {
            if (length(option) == 1) {
                showTickLines[[dimc]] <- list(
                    showTickLines[[dimc]],
                    showTickLines[[dimc]]
                )
                dimc <- dimc + 1
            } else if (length(option) != 2) {
                stop(
                    "Incorrect number of tick line display options. ",
                    "Must be either one, three, or three lists of two."
                )
            }
        }
    } else if (length(showTickLines) == 1) {
        showTickLines <- list(
            list(showTickLines, showTickLines),
            list(showTickLines, showTickLines),
            list(showTickLines, showTickLines)
        )
    } else {
        stop(
            "Incorrect number of tick line display options. ",
            "Must be either one, three, or three lists of two."
        )
    }
    return(showTickLines)
}

#' Check tickLineColors option
#' @description Check the tickLineColors option for consistency and correct it
#' if necessary and possible. This function is used internally by the babyplots
#' package.
#' @param tickLineColors the tickLineColors option
#' @return The corrected tickLineColors option
#' @keywords internal
#' @noRd
check_tickLineColors <- function(tickLineColors) {
    if (length(tickLineColors) == 3) {
        dimc <- 1
        for (option in tickLineColors) {
            if (length(option) == 1) {
                tickLineColors[[dimc]] <- list(
                    tickLineColors[[dimc]],
                    tickLineColors[[dimc]]
                )
                dimc <- dimc + 1
            } else if (length(option) != 2) {
                stop(
                    "Incorrect number of tick line color options. ",
                    "Must be either one, three, or three lists of two."
                )
            }
        }
    } else if (length(tickLineColors) == 1) {
        tickLineColors <- list(
            list(tickLineColors, tickLineColors),
            list(tickLineColors, tickLineColors),
            list(tickLineColors, tickLineColors)
        )
    } else {
        stop(
            "Incorrect number of tick line color options. ",
            "Must be either one, three, or three lists of two."
        )
    }
    return(tickLineColors)
}
