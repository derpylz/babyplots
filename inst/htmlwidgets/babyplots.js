HTMLWidgets.widget({

  name: "babyplots",
  type: "output",

  factory: function (el, width, height) {
    var vis;

    return {

      renderValue: function (x) {
        // create canvas
        el.innerHTML = "";
        var id = el.id;
        var canvasId = id + "-canvas";
        var canvas = document.createElement("canvas");
        if (width) {
          canvas.width = width;
        } else {
          canvas.width = el.offsetWidth;
        }
        if (height) {
          canvas.height = height;
        } else {
          canvas.height = el.offsetHeight;
        }
        canvas.id = canvasId;
        el.appendChild(canvas);
        // create Baby Plots object
        vis = new Baby.Plots(
          canvasId,
          {
            backgroundColor: x.backgroundColor,
            xScale: x.xScale,
            yScale: x.yScale,
            zScale: x.zScale,
            turntable: x.turntable,
            rotationRate: x.rotationRate,
            shapeLegendTitle: x.shapeLegendTitle,
            upAxis: x.upAxis
          }
        );
        vis.R = true;
        document.body.style.backgroundColor = x.backgroundColor;
        for (let plotIdx = 0; plotIdx < x.plots.length; plotIdx++) {
          const plot = x.plots[plotIdx];
          if (["pointCloud", "heatMap", "surface", "shapeCloud", "line"].indexOf(plot.plotType) !== -1) {
            vis.addPlot(
              plot.coords,
              plot.plotType,
              plot.colorBy,
              plot.colorVar,
              {
                name: plot["name"],
                size: plot["size"],
                shape: plot["shape"],
                shading: plot["shading"],
                colorScale: plot["colorScale"],
                customColorScale: plot["customColorScale"],
                colorScaleInverted: plot["colorScaleInverted"],
                sortedCategories: plot["sortedCategories"],
                showLegend: plot["showLegend"],
                legendShowShape: plot["showShape"],
                fontSize: plot["fontSize"],
                fontColor: plot["fontColor"],
                legendTitle: plot["legendTitle"],
                legendTitleFontSize: plot["legendTitleFontSize"],
                legendTitleFontColor: plot["legendTitleFontColor"],
                legendPosition: plot["legendPosition"],
                showAxes: plot["showAxes"],
                axisLabels: plot["axisLabels"],
                axisColors: plot["axisColors"],
                tickBreaks: plot["tickBreaks"],
                showTickLines: plot["showTickLines"],
                tickLineColors: plot["tickLineColors"],
                hasAnimation: plot["hasAnimation"],
                animationTargets: plot["animationTargets"],
                animationDelay: plot["animationDelay"],
                animationDuration: plot["animationDuration"],
                animationLoop: plot["animationLoop"],
                colnames: plot["colnames"],
                rownames: plot["rownames"],
                dpInfo: plot["dpInfo"],
                labels: plot["labels"],
                labelSize: plot["labelSize"],
                labelColor: plot["labelColor"],
                addClusterLabels: plot["addClusterLabels"]
              }
            );
          } else if (plot.plotType === "imageStack") {
            vis.addImgStack(
              plot.values,
              plot.indices,
              plot.attributes,
              {
                name: plot["name"],
                size: plot["size"],
                colorScale: plot["colorScale"],
                showLegend: plot["showLegend"],
                fontSize: plot["fontSize"],
                fontColor: plot["fontColor"],
                legendTitle: plot["legendTitle"],
                legendTitleFontSize: plot["legendTitleFontSize"],
                legendPosition: plot["legendPosition"],
                showAxes: plot["showAxes"],
                axisLabels: plot["axisLabels"],
                axisColors: plot["axisColors"],
                tickBreaks: plot["tickBreaks"],
                showTickLines: plot["showTickLines"],
                tickLineColors: plot["tickLineColors"],
                intensityMode: plot["intensityMode"],
                channelColors: plot["channelColors"],
                channelOpacities: plot["channelOpacities"]
              }
            );
          } else if (plot.plotType === "meshObject") {
            vis.addMeshObject(
              plot.meshString,
              {
                meshScaling: plot["meshScaling"],
                meshRotation: plot["meshRotation"],
                meshOffset: plot["meshOffset"],
              }
            )
          }
        }

        if (x.showUI) {
          vis.createButtons(["json", "label", "publish"]);
        }
        if (x.cameraAlpha !== null && x.cameraAlpha !== undefined) {
          vis.camera.alpha = x.cameraAlpha;
        }
        if (x.cameraBeta !== null && x.cameraBeta !== undefined) {
          vis.camera.beta = x.cameraBeta;
        }
        if (x.cameraRadius !== null && x.cameraRadius !== undefined) {
          vis.camera.radius = x.cameraRadius;
        }
        vis.doRender();
      },

      resize: function (width, height) {
        vis.resize(width, height);
      },

      vis: vis
    }
  }
});